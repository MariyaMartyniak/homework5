package myprojects.automation.assignment5.tests;

import myprojects.automation.assignment5.BaseTest;
import myprojects.automation.assignment5.GeneralActions;
import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.utils.DataConverter;
import myprojects.automation.assignment5.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.Iterator;
import java.util.Set;

public class PlaceOrderTest extends BaseTest {

    @Test
    public void checkSiteVersion() {
        // TODO open main page and validate website version
        driver.get(Properties.getBaseUrl());
//validation can be done by checking the elements present only in mobile version e.g. By mobMenu = By.xpath("//*[@id="menu-icon"]/i"); and create appropriate mobile instance
    }

    @Test(dependsOnMethods = "checkSiteVersion")
    public void createNewOrder() {
        // TODO implement order creation test
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"_desktop_logo\"]/a/img")));
        WebElement allItemsButton = driver.findElement(By.className("all-product-link"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].scrollIntoView(true);", allItemsButton);
        allItemsButton.click();

        // open random product

        GeneralActions generalActions = new GeneralActions(driver);
        generalActions.openRandomProduct();

        // save product parameters
        GeneralActions generalActions1 = new GeneralActions(driver);
        ProductData productData = generalActions1.getOpenedProductInfo();
        /*System.out.println(generalActions1.getOpenedProductInfo());
        System.out.println(productData.getName());
        System.out.println(productData.getQty());
        System.out.println(productData.getPrice());*/
        String productNameExp = productData.getName();
        int productQuantityExp = productData.getQty();
        float productPriceExp = productData.getPrice();


        // add product to Cart and validate product information in the Cart
        WebElement cart = driver.findElement(By.xpath("//div[@class='add']/button"));
        wait.until(ExpectedConditions.elementToBeClickable(cart));
        executor.executeScript("arguments[0].scrollIntoView(true);", cart);
        cart.click();

        String parentWindowHandler = driver.getWindowHandle(); // Store my parent window
        String subWindowHandler = null;
        Set<String> handles = driver.getWindowHandles(); // get all window handles
        Iterator<String> iterator = handles.iterator();
        while (iterator.hasNext()) {
            subWindowHandler = iterator.next();
        }
        driver.switchTo().window(subWindowHandler); // switch to popup window
        // Now the popup window is active
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='cart-content']/a")));
        WebElement frame = driver.findElement(By.xpath("//div[@class='cart-content']/a"));
        frame.click();
        //driver.switchTo().window(parentWindowHandler);  // switch back to parent window


        By orderingButton = By.xpath("//div[@class='text-xs-center']/a");
        wait.until(ExpectedConditions.elementToBeClickable(orderingButton));

        String itemNameAc = driver.findElement(By.xpath("//span[@class='product-image media-middle']/img")).getAttribute("alt");
        String quantity = driver.findElement(By.className("js-cart-line-product-quantity")).getAttribute("value");
        int itemQuantityAc = DataConverter.parseStockValue(quantity);
        String price = driver.findElement(By.tagName("strong")).getText();
        float itemPriceAc = DataConverter.parsePriceValue(price);
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(itemNameAc, productNameExp);
        softAssert.assertEquals(itemQuantityAc, productQuantityExp);
        softAssert.assertEquals(itemPriceAc, productPriceExp);
        softAssert.assertAll();

        driver.findElement(orderingButton).click();

        // proceed to order creation, fill required information
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("checkout-personal-information-step")));
        driver.findElement(By.xpath("//*[@id='customer-form']/section/div[2]/div[1]/input")).sendKeys("CustomerName");
        driver.findElement(By.xpath("//*[@id='customer-form']/section/div[3]/div[1]/input")).sendKeys("CustomerSurname");
        driver.findElement(By.xpath("//*[@id='customer-form']/section/div[4]/div[1]/input")).sendKeys("customernewemailaddress@gmail.com");
        WebElement continueButton = driver.findElement(By.xpath("//*[@id=\"customer-form\"]/footer"));
        executor.executeScript("arguments[0].scrollIntoView(true);", continueButton);
        driver.findElement(By.xpath("//*[@id=\"customer-form\"]/footer/button")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"delivery-address\"]/div/section/div[1]/label")));
        By addressField = By.xpath("//*[@id=\"delivery-address\"]/div/section/div[5]/div[1]/input");
        executor.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(addressField));
        driver.findElement(By.xpath("//*[@id=\"delivery-address\"]/div/section/div[5]/div[1]/input")).sendKeys("StreetName, apt");
        driver.findElement(By.xpath("//*[@id=\"delivery-address\"]/div/section/div[7]/div[1]/input")).sendKeys("79011");
        driver.findElement(By.xpath("//*[@id=\"delivery-address\"]/div/section/div[8]/div[1]/input")).sendKeys("Kiev");
        driver.findElement(By.xpath("//*[@id=\"delivery-address\"]/div/footer/button")).click();
        By nextFromDelivery = By.xpath("//*[@id=\"js-delivery\"]/button");
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(nextFromDelivery)));
        driver.findElement(nextFromDelivery).click();
        By paymentOption2 = By.xpath("//*[@id='payment-option-2']");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"checkout-payment-step\"]/h1")));
        driver.findElement(paymentOption2).click();
        driver.findElement(By.xpath("//*[@id='conditions_to_approve[terms-and-conditions]']")).click();
        By confirmOrderButton = By.xpath("//*[@id='payment-confirmation']/div[1]/button");
        executor.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(confirmOrderButton));
        driver.findElement(confirmOrderButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"_desktop_logo\"]/a/img")));
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"content-hook_order_confirmation\"]/div/div/div/h3")).getText().replace("\uE876", ""), "Ваш заказ подтверждён".toUpperCase());
        // place new order and validate order summary

        softAssert.assertTrue(driver.findElement(By.xpath("//*[@id=\"order-items\"]/div/div/div[2]/span[contains(text(),'" + productNameExp + "')]")).getText().contains(productNameExp));
        softAssert.assertEquals(DataConverter.parsePriceValue(driver.findElement(By.xpath("//*[@id=\"order-items\"]/div/div/div[3]/div/div[1]")).getText()), productPriceExp);
        softAssert.assertEquals(DataConverter.parseStockValue(driver.findElement(By.xpath("//*[@id=\"order-items\"]/div/div/div[3]/div/div[2]")).getText()), productQuantityExp);
        softAssert.assertAll();
        // check updated In Stock value
    }

}
