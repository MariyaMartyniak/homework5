package myprojects.automation.assignment5;


import com.sun.xml.internal.ws.api.server.WebServiceContextDelegate;
import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.utils.DataConverter;
import myprojects.automation.assignment5.utils.logging.CustomReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void openRandomProduct() {
        // TODO implement logic to open random product before purchase
        List<WebElement> allItems = driver.findElements(By.xpath("//div[@id='js-product-list']//article//h1/a"));
        Random random = new Random();
        WebElement randItem = allItems.get(random.nextInt(allItems.size() - 1) + 1);
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].scrollIntoView()", randItem);
        randItem.click();
        //throw new UnsupportedOperationException();
    }

    /**
     * Extracts product information from opened product details page.
     *
     * @return
     */
    public ProductData getOpenedProductInfo() {
        CustomReporter.logAction("Get information about currently opened product");
        // TODO extract data from opened page
        String itemName = driver.findElement(By.xpath("//*[@id=\"wrapper\"]/div/nav/ol/li[5]/a/span")).getText();
        String quantity = driver.findElement(By.id("quantity_wanted")).getAttribute("value");
        int itemQuantity = DataConverter.parseStockValue(quantity);
        String price = driver.findElement(By.xpath("//div[@class='current-price']/span")).getText();
        float itemPrice = DataConverter.parsePriceValue(price);
        ProductData productData = new ProductData(itemName, itemQuantity, itemPrice);
        return productData;
        //throw new UnsupportedOperationException();
    }


}

